<?php


namespace WPezSCBoilerplate\App\Core\Admin\Cleanup\Posts;


// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

if ( ! class_exists( 'ClassPosts' ) ) {
    class ClassPosts {

        protected $_arr_post_row_actions_post_types;
        protected $_arr_post_row_actions;


        public function __construct() {

            $this->setPropertyDefaults();

        }


        protected function setPropertyDefaults() {

            $this->_arr_post_row_actions_post_types = 'all';

            $this->_arr_post_row_actions = [
                'edit'                 => true,
                'inline hide-if-no-js' => true,   // aka quick edit
                'view'                 => true,
                'trash'                => true,
                'untrash'              => true, // aka restore
                'delete'               => true, // aka perm delete
            ];
        }

        public function setPostRowActionsPostTypes( $arr ) {

            if ( is_array( $arr ) ) {
                $this->_arr_post_row_actions_post_types = $arr;
            }

        }

        public function setPostRowActions( $arr ) {

            if ( is_array( $arr ) ) {
                $this->_arr_post_row_actions = $arr;
            }

        }

        public function updatePostRowActions( $arr ) {

            if ( is_array( $arr ) ) {
                $this->_arr_post_row_actions = array_merge( $this->_arr_post_row_actions, $arr );
            }

        }


        /*         *
         * add_filter( 'bulk_actions-edit-post', [ TODO, 'bulkActionsEditRemoveSingle'] );
         *
         * add_filter( 'bulk_actions-upload', [ TODO, 'bulkActionsEditRemoveSingle'] );
         *
         * add_filter( 'bulk_actions-edit-$post_type', [ TODO, 'bulkActionsEditRemoveSingle'] );
         *
         */
        public function bulkActionsEditRemoveSingle( $arr_actions = [] ) {

            unset ( $arr_actions['edit'] );

            return $arr_actions;
        }


        /**
         * Will remove Edit from select: Bulk Actions for All screens where screen_base === 'edit'
         *
         * add_filter('current_screen',[ TODO, 'bulkActionsEditRemove'] );
         *
         * @param $obj_screen
         *
         * @return mixed
         */
        public function bulkActionsEditRemove( $obj_screen ) {

            if ( ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
                return $obj_screen;
            }

            // Note: This breaks ez pattern but for convenience it's fine. (For now?)
            if ( $obj_screen->base === 'edit' ) {

                add_filter( 'bulk_actions-' . $obj_screen->id, [ $this, 'bulkActionsEditRemoveSingle' ] );
            }

            return $obj_screen;

        }



        public function postRowActionsRemove( $arr_actions ) {

            global $post;

            if ( $this->_arr_post_row_actions_post_types == 'all'
                 || ( isset( $this->_arr_post_row_actions_post_types[ $post->post_type ] )
                      && $this->_arr_post_row_actions_post_types[ $post->post_type ] === true ) ) {

                foreach ( $this->_arr_post_row_actions as $str => $bool ) {

                    if ( $bool !== false ) {

                        unset( $arr_actions[ $str ] );
                    }
                }
            }

            return $arr_actions;
        }


    }
}