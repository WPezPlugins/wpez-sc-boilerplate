<?php

/**
 * As inspired by:
 *
 * https://wpdreamer.com/2014/04/how-to-make-your-wordpress-admin-columns-sortable/
 *
 * https://github.com/bamadesigner/manage-wordpress-posts-using-bulk-edit-and-quick-edit
 *
 * This class is for simple __display__. it does not implement the quick edit
 * functionality of the above. that might come in another class at a later date
 *
 */


namespace WPezSCBoilerplate\App\Core\Admin\Posts\Columns;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

if ( ! class_exists( 'ClassColumns' ) ) {
    class ClassAddDisplay {

        protected $_str_wp_field;
        protected $_str_col_title;
        protected $_int_add_col_index;

        protected $_str_meta_type;
        protected $_str_col_width;
        protected $_bool_edit_post_link;

        protected $_str_post_type;
        protected $_str_prefix;
        protected $_arr_meta_type;
        protected $_str_order;
        protected $_str_wp_field_is;

        protected $_arr_core;
        protected $_str_orderby_core;

        protected $_arr_special;
        protected $_str_orderby_special_asc;
        protected $_str_orderby_special_desc;

        protected $_arr_custom_column_display;


        public function __construct() {

            $this->setPropertyDefaults();
        }


        protected function setPropertyDefaults() {

            $this->_str_post_type   = false;
            $this->_str_prefix      = 'column_';
            $this->_str_wp_field    = false;
            $this->_str_wp_field_is = 'core';

            //  'CHAR', 'NUMERIC', 'BINARY', 'CHAR', 'DATE', 'DATETIME', 'DECIMAL', 'SIGNED', 'TIME', 'UNSIGNED'
            //  https://codex.wordpress.org/Class_Reference/WP_Query
            $this->_str_meta_type       = 'CHAR';
            $this->_str_col_width       = false;
            $this->_bool_edit_post_link = false;

            $this->_arr_meta_type = [

                'NUMERIC',
                'BINARY',
                'CHAR',
                'DATE',
                'DATETIME',
                'DECIMAL',
                'SIGNED',
                'TIME',
                'UNSIGNED'
            ];

            $this->_str_col_title     = '';
            $this->_int_add_col_index = false;

            $this->_str_order = 'ASC';
            $this->_arr_core  = [

                // 'none',
                'ID',
                'post_author',
                'post_title',
                'post_name',
                'post_type',
                'post_date',
                'post_modified',
                'post_parent',
                // 'rand',
                'comment_count',
                // 'relevance',
                'menu_order',
                //  'meta_value',
                //  'meta_value_num',
                //  'post__in',
                //  'post_name__in',
                //  'post_parent__in'
            ];

            $this->_str_orderby_core         = false;
            $this->_str_orderby_special_asc  = false;
            $this->_str_orderby_special_desc = false;

            $this->_arr_special = [
                'post_excerpt',
                'post_content',
                'post_status',
                'comment_status'
            ];

            $this->_arr_custom_column_display = false;
        }


        public function setWPField( $str = false ) {

            if ( is_string( $str ) ) {

                $this->_str_wp_field = $str;

                $this->setWPFieldIs();

                return true;
            }

            return false;
        }


        protected function setWPFieldIs() {

            if ( in_array( $this->_str_wp_field, $this->_arr_core ) ) {

                $this->_str_wp_field_is = 'core';

            } elseif ( in_array( $this->_str_wp_field, $this->_arr_special ) ) {

                $this->_str_wp_field_is = 'special';

            } else {

                $this->_str_wp_field_is = 'meta';
            }
        }


        public function setColumnTitle( $str = false ) {

            if ( is_string( $str ) ) {

                $this->_str_col_title = $str;

                return true;
            }

            return false;
        }


        /**
         * Where do you want the new col added? If other code is adding cols this could be impacted by the priority of this action.
         *
         * The checkbox ('cb') is column 0;
         *
         * @param bool $int
         *
         * @return bool
         */
        public function setAddColumnIndex( $int = false ) {

            if ( is_integer( $int ) ) {

                $this->_int_add_col_index = $int;

                return true;

            }

            return false;
        }


        public function setColumnWidth( $str = false ) {

            if ( is_string( $str ) ) {

                $this->_str_col_width = $str;

                return true;
            }

            return false;
        }


        public function setEditPostLink( $bool = false ) {

            if ( is_bool( $bool ) ) {

                $this->_bool_edit_post_link = $bool;

                return true;
            }

            return false;
        }


        public function setMetaType( $str = false ) {

            if ( in_array( strtoupper( $str ), $this->_arr_meta_type ) ) {

                $this->_str_meta_type = strtoupper( $str );

                return true;
            }

            return false;
        }


        public function setOrderbyCore( $str = false ) {

            if ( is_string( $str ) ) {

                $this->_str_orderby_core = $str;

                return true;
            }

            return false;
        }

        public function setOrderbySpecialAsc( $str = false ) {

            if ( is_string( $str ) ) {

                $this->_str_orderby_special_asc = $str;

                return true;
            }

            return false;
        }

        public function setOrderbySpecialDesc( $str = false ) {

            if ( is_string( $str ) ) {

                $this->_str_orderby_special_desc = $str;

                return true;
            }

            return false;
        }

        /**
         * Set an obj + method that will be used in managePostsCustomColumnDisplay(), else
         * we'll just use the basic default display.
         *
         * @param bool $arr
         */
        public function injectCustomColumnDisplay( $arr = false ) {

            if ( is_array( $arr ) && isset( $arr[0], $arr[1] ) && method_exists( $arr[0], $arr[1] ) ) {

                $this->_arr_custom_column_display = $arr;
            }
        }

        /**
         * https://codex.wordpress.org/Plugin_API/Filter_Reference/manage_posts_columns
         *
         * add_filter( 'manage_$post_type_posts_columns', [ TODO, 'addColumn'])
         *
         * or for ALL post types...
         * add_filter( 'manage_posts_columns', ...call this method)
         */
        public function addColumn( $arr_cols ) {

            if ( $this->_str_wp_field === false ) {
                return $arr_cols;
            }

            // we'll "cheat"  and grab the post_type (instead of setting it. the hook is our gatekeeper
            global $post;
            $this->_str_post_type = $post->post_type;

            if ( $this->_int_add_col_index === false ) {
                $this->_int_add_col_index = count( $arr_cols );
            }

            $arr_new = array_slice( $arr_cols, 0, $this->_int_add_col_index, true ) +
                       [ $this->_str_prefix . $this->_str_wp_field => $this->_str_col_title ] +
                       array_slice( $arr_cols, $this->_int_add_col_index, null, true );

            return $arr_new;

        }

        // add_filter( 'manage_$post_type_sortable_columns', [ TODO, 'makeSortable ]);
        public function makeSortable( $arr_sortable_cols ) {

            if ( $this->_str_wp_field !== false ) {

                $arr_sortable_cols[ $this->_str_prefix . $this->_str_wp_field ] = $this->_str_wp_field;
            }

            return $arr_sortable_cols;
        }


        /**
         * add_action( 'pre_get_posts', [ TODO, 'preGetPosts']);
         */
        public function preGetPosts( $query ) {

            if ( $this->_str_wp_field !== false ) {
                /**
                 * We only want our code to run in the main WP query
                 * AND if an orderby query variable is designated.
                 */
                {
                    if ( $query->is_main_query() && ( $query->get( 'orderby' ) ) && is_admin() ) {

                        $str_orderby = $query->get( 'orderby' );

                        // TODO what does orderby look like when orderby'ing 2 or more fields?
                        if ( $str_orderby === $this->_str_wp_field ) {

                            if ( $this->_str_wp_field_is == 'core' ) {

                                $query->set( 'orderby', $this->_str_wp_field );
                                if ( $this->_str_orderby_core !== false ) {
                                    $query->set( 'orderby', $this->_str_orderby_core );
                                }

                            } elseif ( $this->_str_wp_field_is == 'special' ) {

                                if ( $query->get( 'order' ) ) {

                                    $this->_str_order = $query->get( 'order' );

                                    add_filter( 'posts_orderby', [ $this, 'postsOrderbyFilter' ] );
                                }

                            } else {
                                // meta

                                // set our query's meta_key, which is used for custom fields
                                $query->set( 'meta_key', $this->_str_wp_field );

                                /**
                                 * If your meta value are numbers, change
                                 * 'meta_value' to 'meta_value_num'.
                                 */

                                $query->set( 'orderby', 'meta_value' );

                                // https://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters
                                //
                                // "You may also specify 'meta_type' if you want to cast the meta value as a specific type. Possible values are:
                                // 'NUMERIC', 'BINARY', 'CHAR', 'DATE', 'DATETIME', 'DECIMAL', 'SIGNED', 'TIME', 'UNSIGNED',
                                // same as in '$meta_query'."
                                $query->set( 'meta_type', $this->_str_meta_type );

                            }


                        }
                    }
                }
            }
        }


        /**
         * https://codex.wordpress.org/Class_Reference/WP_Query#Filters
         *
         * TODO > use this instead?
         *
         * @param $str_orderby_statement
         *
         * @return string
         */
        public function postsOrderbyFilter( $str_orderby_statement ) {

            if ( $this->_str_orderby_special_asc !== false && $this->_str_orderby_special_desc !== false ) {

                // TODO - this is TRICKY. how do we know which is which if there's more than one orderby / order?
                $str_orderby_statement = $this->_str_orderby_special_desc;
                if ( strtoupper( $this->_str_order ) == 'ASC ' ) {

                    $str_orderby_statement = $this->_str_orderby_special_asc;

                }

            } else {

                $str_orderby_statement = $this->_str_wp_field . ' ' . $this->_str_order;
            }

            return $str_orderby_statement;
        }

        /**
         * Ultra-basic method for displaying the new col's field. if you want anything more elaborate, either use the filter or just not use this method.
         *
         * https://codex.wordpress.org/Plugin_API/Action_Reference/manage_posts_custom_column
         *
         * add_filter( 'manage_{$post_type}_posts_custom_column', [TODO, 'managePostsCustomColumn'], 10, 2);
         * add_filter( 'manage_posts_custom_column', [TODO, 'managePostsCustomColumn'], 10, 2);
         */

        public function managePostsCustomColumnDisplay( $str_column_index, $post_id ) {

            if ( $str_column_index == $this->_str_prefix . $this->_str_wp_field ) {

                if ( $this->_arr_custom_column_display !== false ) {

                    $obj    = $this->_arr_custom_column_display[0];
                    $method = $this->_arr_custom_column_display[1];

                    $obj->$method( $str_column_index, $post_id );

                } else {

                    if ( $this->_str_wp_field_is == 'core' || $this->_str_wp_field_is == 'special' ) {

                        $obj_post = get_post( $post_id );
                        $str_property = $this->_str_wp_field;
                        $str_field_value = $obj_post->$str_property;

                        $this->echoField($str_field_value);

                    } else {

                        $str_field_value = get_post_meta( $post_id, $this->_str_wp_field, true );
                        $this->echoField($str_field_value);

                    }


                }

            }
        }


        protected function echoField( $str_field_value = '' ){

            if ( $this->_bool_edit_post_link === true ) {

                edit_post_link( $str_field_value, '<p>', '</p>' );

            } else {

                echo  esc_html( $str_field_value );
            }


        }


        /*
         * 	add_action('admin_footer', ['TODO', 'styleColumn'] );
         */
        public function styleColumn() {

            if ( $this->_str_col_width !== false ) {
                global $post;
                if ( $post->post_type == $this->_str_post_type ) {
                    // TODO - the class slug might vary by field type. we'll see.
                    echo '<style>.column-column_' . esc_attr( $this->_str_wp_field ) . '{width: ' . esc_attr( $this->_str_col_width ) . '}</style>';
                }
            }
        }

    }
}