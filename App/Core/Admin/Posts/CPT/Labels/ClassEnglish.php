<?php

namespace WPezSCBoilerplate\App\Core\Admin\Posts\CPT\Labels;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

if ( ! class_exists( 'ClassEnglish' ) ) {
    class ClassEnglish {
        
        protected $_str_singular;
        protected $_str_plural;

        public function __construct( $arr_args = array() ) {

            $this->setPropertyDefaults();

        }

        protected function setPropertyDefaults(){
            
            $this->_str_singular = false;
            $this->_str_str_plural = false;

        }

        public function setSingular( $str = false ){

            $str = esc_attr($str);
            if ( is_string($str) && ! empty( $str ) ){
                $this->_str_singular = $str;
                return true;
            }
            return false;
        }

        public function setPlural( $str = false ){
            
            $str = esc_attr($str);
            if ( is_string($str) && ! empty( $str ) ){
                $this->_str_plural = $str;
                return true;
            }
            return false;
        }

        function getLabels() {
            
            
            if ( $this->_str_singular === false || $this->_str_plural === false ){
                return [];
            }

            $arr_labels = array(
                // 'name' - general name for the post type, usually plural. The same and overridden by $post_type_object->label. Default is Posts/Pages
                'name'                  => $this->_str_plural,

                //  name for one object of this post type. Default is Post/Page
                'singular_name'         => $this->_str_singular,

                // the add new text. The default is "Add New" for both hierarchical and non-hierarchical post types. When internationalizing this string,
                // please use a gettext context matching your post type. Example: _x('Add New', 'product');
                'add_new'               => 'Add New',

                // Default is Add New Post/Add New Page.
                'add_new_item'          => 'Add New' . ' ' . $this->_str_singular,

                // Default is Edit Post/Edit Page.
                'edit_item'             => 'Edit' . ' ' . $this->_str_singular,

                // Default is New Post/New Page.
                'new_item'              => 'New' . ' ' . $this->_str_singular,

                //  Default is View Post/View Page.
                'view_item'             => 'View' . ' ' . $this->_str_singular,

                // Label for viewing post type archives. Default is 'View Posts' / 'View Pages'.
                'view_items'            => 'View' . ' ' . $this->_str_plural,

                //  Default is Search Posts/Search Pages.
                'search_items'          => 'Search' . ' ' . $this->_str_plural,

                //  Default is No posts found/No pages found.
                'not_found'             => 'Sorry. Search found no' . ' ' . $this->_str_plural,

                // Default is No posts found in Trash/No pages found in Trash.
                'not_found_in_trash'    => 'Not found in Trash',

                // This string isn't used on non-hierarchical types. In hierarchical ones the default is 'Parent Page:'.
                'parent_item_colon'     => $this->_str_singular . ':',

                // String for the submenu. Default is All Posts/All Pages.
                'all_items'             => 'All' . ' ' . $this->_str_plural,

                // String for use with archives in nav menus. Default is Post Archives/Page Archives.
                'archives'              => $this->_str_singular . ' ' . 'Archives',

                // Label for the attributes meta box. Default is 'Post Attributes' / 'Page Attributes'.
                'attributes'            => $this->_str_singular . ' ' . 'Attributes',

                // String for the media frame button. Default is Insert into post/Insert into page.
                'insert_into_item'      => 'Insert into this' . ' ' . $this->_str_singular,

                //  String for the media frame filter. Default is Uploaded to this post/Uploaded to this page
                'uploaded_to_this_item' => 'Uploaded to this' . ' ' . $this->_str_singular,

                // Default is Featured Image.
                'featured_image'        => 'Featured Image',

                // Default is Set featured image.
                'set_featured_image'    => 'Set featured image',

                // Default is Remove featured image.
                'remove_featured_image' => 'Remove featured image',

                // Default is Use as featured image.
                'use_featured_image'    => 'Use as featured image',

                // Default is the same as `name`.
                'menu_name'             => $this->_str_plural,

                // String for the table views hidden heading.
                'filter_items_list'     => $this->_str_plural . ': Filter list',

                // String for the table pagination hidden heading.
                'items_list_navigation' => $this->_str_plural . ' ' .  'list navigation',

                // String for the table hidden heading.
                'items_list'            => $this->_str_plural . ' ' . 'list',

                // String for use in New in Admin menu bar. Default is the same as `singular_name`.
                'name_admin_bar'        => $this->_str_plural,
            );

            return $arr_labels;
        }

    }
}