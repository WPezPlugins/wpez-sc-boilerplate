<?php

namespace WPezSCBoilerplate\App;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

use WPezSCBoilerplate\App\Setup\Admin\Cleanup\Posts\ClassCleanup;
use WPezSCBoilerplate\App\Setup\Admin\Posts\CPT\ClassCPT;

use WPezSCBoilerplate\App\Setup\MVC\Controller\ClassController;

class ClassPlugin {

    protected $_str_post_type;
    protected $_str_shortcode_name;

    public function __construct() {

        $this->setPropertyDefaults();

        $this->CPT();

        $this->Cleanup();

        $this->addShortcode();
    }


    protected function setPropertyDefaults() {

        $this->_str_post_type      = 'wpez_scbp';
        $this->_str_shortcode_name = 'wpez_scbp';
    }


    protected function CPT() {

        $new = new ClassCPT( $this->_str_post_type );

    }

    protected function Cleanup() {

        $new = new ClassCleanup( $this->_str_post_type );

    }

    protected function addShortcode() {

        $new = new ClassController();

        $new->setPostType( $this->_str_post_type );
        $new->setDefaultWrapperTag('div');
        $new->setDefaultWrapperClass('wpez-scbp');
        $new->setDefaultWrapperIDSlug('wpez-scbp-id-');

        add_shortcode( $this->_str_shortcode_name, [ $new, 'shortcodeController' ] );
    }
}