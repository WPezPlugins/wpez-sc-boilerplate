<?php

namespace WPezSCBoilerplate\App\Setup\Admin\Cleanup\Posts;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

use WPezSCBoilerplate\App\Core\Hooks\Register\ClassRegister as HooksRegister;
use WPezSCBoilerplate\App\Core\Admin\Cleanup\Posts\ClassPosts as CleanupPosts;

class ClassCleanup {

    protected $_str_post_type;

    // protected $_arr_actions;
    protected $_arr_filters;

    public function __construct( $str_post_type = false ) {

        if ( ! is_string( $str_post_type ) ) {
            return false;
        }
        $this->_str_post_type = $str_post_type;

        $this->setPropertyDefaults();

        $this->adminCleanup();

        // this should be last
        $this->hooksRegister();

    }

    protected function setPropertyDefaults( ) {

        // $this->_arr_actions   = [];
        $this->_arr_filters   = [];
    }

    /**
     * After gathering (below) the arr_actions and arr_filter, it's time to
     * make some RegisterHook magic
     */
    protected function hooksRegister() {

        $new = new HooksRegister();;

        // $new->loadActions( $this->_arr_actions );

        $new->loadFilters( $this->_arr_filters );

        $new->doRegister();

    }

    public function adminCleanup() {

        $new = new CleanupPosts();

        //  ALL = add_filter('current_screen',[ $new, 'bulkActionsEditRemove'] );
        //  add_filter( 'bulk_actions-edit-' . $this->_str_post_type, [ $new, 'bulkActionsEditRemoveSingle'] );
        //  add_filter( 'bulk_actions-upload', [ $new, 'bulkActionsEditRemoveSingle'] );

        $this->_arr_filters[] = (object)[
            'hook'      => 'bulk_actions-edit-' . $this->_str_post_type,
            'component' => $new,
            'callback'  => 'bulkActionsEditRemoveSingle',
            'priority'  => 10
        ];

        //  'hook'  => 'post_row_actions' can't be limited to a post type (like 'bulk_actions-edit-') we have to set the post types here
        // and the class will enforce the rule
        $new->setPostRowActionsPostTypes( [ $this->_str_post_type => true ] );

        //  add_filter('post_row_actions',[$new, 'postRowActionsRemove'],10,1);
        $this->_arr_filters[] = (object)[
            'hook'      => 'post_row_actions',
            'component' => $new,
            'callback'  => 'postRowActionsRemove',
            'priority'  => 100
        ];
    }

}