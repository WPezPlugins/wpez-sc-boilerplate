<?php
/**
 * Created by PhpStorm.
 */

namespace WPezSCBoilerplate\App\Setup\MVC\View;

class ClassView {

    use \WPezSCBoilerplate\App\Core\Traits\MarkupGeneration\TraitMarkupGeneration;

    protected $_bool_wrapper_active;
    protected $_str_wrapper_semantic_tag;
    protected $_str_wrapper_tag;
    protected $_str_wrapper_id;
    protected $_str_wrapper_class;
    protected $_str_content;

    public function __construct() {

        $this->setPropertyDefaults();

    }

    protected function setPropertyDefaults() {

        $this->_bool_wrapper_active      = true;

        // we init these to a bool but ultimatel they are str_
        $this->_str_wrapper_semantic_tag = false;
        $this->_str_wrapper_tag          = false;
        $this->_str_wrapper_id           = false;
        $this->_str_wrapper_class        = false;
        $this->_str_content              = false;
    }


    public function getView() {

        $str_ret = '';
        if ( $this->_bool_wrapper_active !== true ) {
            return $str_ret;
        }

        // TODO - correct?
        if ( empty( wp_kses_post_deep( $this->_str_content ) ) ) {
            return $str_ret;
        }

        $arr_global_attrs = [];
        if ( $this->_str_wrapper_tag !== false ) {

            if ( $this->_str_wrapper_id !== false ) {
                $arr_global_attrs['id'] = $this->_str_wrapper_id;
            }

            if ( $this->_str_wrapper_class !== false ) {
                $arr_global_attrs['class'] = $this->_str_wrapper_class;
            }
        }

        $obj_vw_args = (object) [
            'active' => true,
            'semantic_tag' => $this->_str_wrapper_semantic_tag,
            'semantic_global_attrs' => false,
            'view_wrapper_tag' => $this->_str_wrapper_tag,
            'view_wrapper_global_attrs' => $arr_global_attrs,
        ];

        $obj_vw = $this->viewWrapper($obj_vw_args);

        // TODO - is  wp_kses_post_deep() necessary? Won't the theme do this? (Or at least should?)
        return  $obj_vw->semantic_open .  $obj_vw->view_wrapper_open . wp_kses_post_deep( $this->_str_content ) . $obj_vw->view_wrapper_close. $obj_vw->semantic_close;
    }

    public function setWrapperActive( $bool = true ) {

        if ( is_bool( $bool ) ) {
            $this->_bool_wrapper_active = $bool;

            return true;
        }

        return false;
    }

    public function setWrapperSemanticTag( $str = false ) {

        if ( is_string( $str ) && ! empty( esc_attr( $str ) ) ) {
            $this->_str_wrapper_semantic_tag = $str;

            return true;
        }

        return false;
    }

    public function setWrapperTag( $str = false ) {

        if ( is_string( $str ) && ! empty( esc_attr( $str ) ) ) {
            $this->_str_wrapper_tag = $str;

            return true;
        }

        return false;
    }

    public function setWrapperID( $str = false ) {

        if ( is_string( $str ) && ! empty( esc_attr( $str ) ) ) {
            $this->_str_wrapper_id = $str;

            return true;
        }

        return false;
    }

    public function setWrapperClass( $str = false ) {

        if ( is_string( $str ) && ! empty( esc_attr( $str ) ) ) {
            $this->_str_wrapper_class = $str;

            return true;
        }

        return false;
    }

    public function setContent( $str = false ) {

        if ( is_string( $str ) && ! empty( wp_kses_post( $str ) ) ) {
            $this->_str_content = $str;

            return true;
        }

        return false;
    }
}