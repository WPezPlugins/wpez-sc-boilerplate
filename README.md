## WPezPlugins: Shortcode Boilerplate

__A WordPress plugin that uses a custom post type (admin menu name: Boilerplate) to store boilerplate content. You can add that content in other posts / pages via shortcode.__ 

The shortcode has some additional atts for (minor) customizations.


> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### OVERVIEW

Currently, there is one shortcode: 

##### 1 - [wpez_scbp]

The valid shortcode atts are:

- __id__ | REQUIRED | Default: none - The (post) ID of the Boilerplate entry. Example: id="123." The post_type will be checked to make sure this id is for a Boilerplate post.

- __semantic_tag__ | OPTIONAL | Default: none - The HTML5 semantic tag that will wrap everything. 

   - Please note: 

   - There's no validation (only sanitization). Therefore, (e.g.) semantic_tag="junk" would render:
    ```<junk>...</junk>``` The browser will ignore that, but it's possible in theory. 
  
   - That said, nothing forces this to be a semantic tag, so (e.g.) "div" or "span" would also render.
   
   - There are no global attributes for semantic_tag.

   
- __wrapper_tag__ | OPTIONAL | Default =  "div" - The html tag what's within the semantic tag (if that's specified), but wraps the boilerplate content.

- __wrapper_id_slug__ | OPTIONAL | Default =  "wpez-scbp-id-" - The default behavior is to auto-append the id to this slug and render the wrapper_tag's id as (e.g.): id="wpez-scbp-id-123".

  - Please note: 
  
  - You can specify your own slug (e.g.) wrapper_id_slug="my-custom-id-slug-".
  
  - If you don't want any id at all then use wrapper_id_slug="0". That "0" is a zero. 

  
- __wrapper_id__ : OPTIONAL | Default: none - If you don't want to use the wrapper_id_slug (with auto-append of the id), you can specify a custom wrapper_id. The wrapper_id will override the slug + auto-append behavior. 

- __wrapper_class__  - OPTIONAL | Default = wpez-scbp - You can specify your own class, or use the default. Currently, this is no way to stop the class from being added. See TODOs. 

> --
>
>__WARNING!!!__
> 
>__You can have one SC Boilerplate post contain a shortcode referencing another SC Boilerplate post.__ 
>
>__Make sure you don't get caught in an infinte loop.__ 
>
> --


### FAQ

__1 - Why?__

Because I just spent too much time rebuilding a site that used V-sual C-mposer. Page after page after page had a lot of the same content, and the only way to update that (e.g., change in contact name) was to do so page by page. 

Note: Maybe there was a better way in VC to do this, but I didn't do the original build. At this point, VC has been removed. 



### HELPFUL LINKS

- https://themehybrid.com/weblog/how-to-apply-content-filters
 
 
### TODO

- Add wrapper_class="0" so the class doesn't render at all. 
- Investigate the need for using wp_kses_post_deep() in the view. The theme should be taking care of this. Double escaping could get messy. Maybe?

### CHANGE LOG


__-- 0.0.1__

- INIT - And away we go...


