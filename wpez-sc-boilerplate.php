<?php
/*
Plugin Name: WPezPlugins: Shortcode Boilerplate
Plugin URI: https://gitlab.com/WPezPlugins/wpez-sc-boilerplate
Description: Uses a custom post type (admin menu name: Boilerplate) to store boilerplate content. You can add that content in other posts / pages via shortcode.
Version: 0.0.1
Author: Mark Simchock for Alchemy United
Author URI: http://AlchemyUnited.com
License: GPLv2+
Text Domain: wpez_sc-boilerplate
Domain Path: /languages
*/

namespace WPezSCBoilerplate;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

use WPezSCBoilerplate\App\ClassPlugin;

$str_php_ver_comp = '5.4.0';

// we reserve the right to use traits :)
if (version_compare(PHP_VERSION, $str_php_ver_comp, '<')) {
    exit(sprintf('WPezPlugins: Shortcode Markup requires PHP ' . esc_html($str_php_ver_comp) . ' or higher. Your WordPress site is using PHP %s.', PHP_VERSION));
}

function autoloader( $bool = true ){

    if ( $bool !== true ) {
        return;
    }

    require_once 'App/Core/Autoload/ClassAutoload.php';

    $new_autoload = new ClassAutoload();

    $new_autoload->setPathBase(plugin_dir_path( __FILE__ ));
    $new_autoload->setNeedleRoot(__NAMESPACE__ );
    $new_autoload->setNeedleChild('App');

    spl_autoload_register( [$new_autoload, 'wpezAutoload'], true );
}
autoloader();

function plugin($bool = true){

    if ( $bool !== true ) {
        return;
    }

    $new_plugin = new ClassPlugin();
}
plugin();